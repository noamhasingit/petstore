from flask import Flask
from main import petstore
app = Flask(__name__)


@app.route('/')
def hello():
    return "Hello HCL!"

@app.route('/getpets')
def getPets():
    myPet = petstore()
    petsRes = myPet.get_available_pets()
    r_dictionary = petsRes.json()
    for i in range(len(r_dictionary)):
        myPet.save_pets_to_db(r_dictionary[i])
    return str(myPet.read_pets_from_db())
    

def run(host='0.0.0.0',port='8080'):
    app.run(host='0.0.0.0',port='8080')

if __name__ == '__main__':
    app.run(host='0.0.0.0',port='8080')
