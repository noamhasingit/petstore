import requests
from pymongo import MongoClient
from flask import Flask



class petstore:


    def __init__(self,url='https://petstore.swagger.io/',dburl='mongodb://localhost:27017/'):
      
        self.mongoClient = MongoClient(dburl)
        self.petsDB = self.mongoClient['pets']
        self.petsCollection = self.petsDB.Collection
        self.url = url

    def get_available_pets(self):
        target_url =  str(self.url) + 'v2/pet/findByStatus?status=available'
        r = requests.get(target_url)
        return r

    def save_pets_to_db(self,document):
        self.petsCollection.insert(document)

    def read_pets_from_db(self,filter={}):
        colList = []
        for collection in self.petsCollection.find(filter):
            colList.append(collection)
        return colList      



if __name__ == '__main__':
    myPet = petstore()
    petsRes = myPet.get_available_pets()
    r_dictionary = petsRes.json()

    for i in range(len(r_dictionary)):
        myPet.save_pets_to_db(r_dictionary[i]) 
        #print(r_dictionary[i])
    
    print(myPet.read_pets_from_db())


